<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\Admin\BooksController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\AuthorsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the
 RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [App\Http\Controllers\Front\FrontController::class, 'index']);
// Route::get('/',[App\Http\Controllers\Front\FrontController::class,'index']);
Route::controller(AuthController::class)->group(function () {
    Route::get('register', 'register')->name('register');
    Route::post('register', 'registerSave')->name('register.save');

    Route::get('login', 'login')->name('login');
    Route::post('login', 'loginAction')->name('login.action');

    Route::get('logout', 'logout')
        ->middleware('auth')
        ->name('logout');
});

Route::middleware('auth')->group(function () {
    Route::get('dashboard', function () {
        return view('admin.dashboard');
    })->name('dashboard');

    Route::controller(BooksController::class)
        ->prefix('books')
        ->group(function () {
            Route::get('', 'index')->name('books');
            Route::get('create', 'create')->name('books.create');
            Route::post('store', 'store')->name('books.store');
            Route::get('show/{id}', 'show')->name('books.show');
            Route::get('edit/{id}', 'edit')->name('books.edit');
            Route::put('edit/{id}', 'update')->name('books.update');
            Route::delete('destroy/{id}', 'destroy')->name('books.destroy');
        });
    Route::controller(AuthorsController::class)
        ->prefix('authors')
        ->group(function () {
            Route::get('', 'index')->name('authors');
            Route::get('create', 'create')->name('authors.create');
            Route::post('store', 'store')->name('authors.store');
            Route::get('show/{id}', 'show')->name('authors.show');
            Route::get('edit/{id}', 'edit')->name('authors.edit');
            Route::put('edit/{id}', 'update')->name('authors.update');
            Route::delete('destroy/{id}', 'destroy')->name('authors.destroy');
        });
    Route::controller(CategoryController::class)
        ->prefix('categories')
        ->group(function () {
            Route::get('', 'index')->name('categories');
            Route::get('create', 'create')->name('categories.create');
            Route::post('store', 'store')->name('categories.store');
            Route::get('show/{id}', 'show')->name('categories.show');
            Route::get('edit/{id}', 'edit')->name('categories.edit');
            Route::put('edit/{id}', 'update')->name('categories.update');
            Route::delete('destroy/{id}', 'destroy')->name('categories.destroy');
        });

    Route::get('/profile', [App\Http\Controllers\Auth\AuthController::class, 'profile'])->name('profile');
});

<!-- header -->
<header class="banner overlay bg-cover" data-background="images/banner.jpg">
    <nav class="navbar navbar-expand-md navbar-dark">
        <div class="container">
            <a class="navbar-brand px-2" href="index.html">Elektron kutubxona xizmati</a>
            <button class="navbar-toggler border-0" type="button" data-toggle="collapse" data-target="#navigation"
                aria-controls="navigation" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse text-center" id="navigation">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link text-dark" href="/">Asosiy sahifa</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-dark" href="/">Savol-javob</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-dark" href="/">Fikr mulohazalar</a>
                    </li>
                    <li class="nav-item">
                        <!-- <a class="nav-link text-dark" href="single.html">Inner Page</a> -->
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- banner -->
    <div class="container section">
        <div class="row">
            <div class="col-lg-8 text-center mx-auto">
                <h1 class="text-white mb-3">
                    Toshkent kimyo-texnologiya instituti Shahrisabz filiali
                </h1>
                <p class="text-white mb-4">Elektron kutubxonaga xush kelibsz</p>
                <div class="position-relative">
                    <input id="search" class="form-control" placeholder="Kitob nomi yoki muallifini kiriting"><i
                        class="ti-search search-icon"></i>
                </div>
            </div>
        </div>
    </div>
    <!-- /banner -->
</header>
<!-- /header -->

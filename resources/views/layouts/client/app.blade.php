<!DOCTYPE html>
<html lang="en-us">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Kimyo texnologiya markazi</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- theme meta -->
    <meta name="theme-name" content="dot" />

    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{ asset('front_assets/plugins/bootstrap/bootstrap.min.css') }}">

    <!-- themefy-icon -->
    <link rel="stylesheet" href="{{ asset('front_assets/plugins/themify-icons/themify-icons.css') }}">

    <!--Favicon-->
    <link rel="icon" href="{{ asset('front_assets/images/favicon.png') }}" type="image/x-icon">

    <!-- fonts -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700&display=swap" rel="stylesheet">

    <!-- Main Stylesheet -->
    <link href="{{ asset('front_assets/style/style.css') }}" rel="stylesheet" media="screen" />
    @yield('style')
</head>

<body>
    <!-- header -->
    @include('layouts.client.navbar')

    <!-- /header -->

    <!-- topics -->
    @yield('content')
    <!-- footer -->
    @include('layouts.client.footer')

    <!-- /footer -->
    <!-- jquiry -->
    <script src="{{ asset('front_assets/plugins/jquery/jquery-1.12.4.js') }}"></script>
    <!-- Bootstrap JS -->
    <script src="{{ asset('front_assets/plugins/bootstrap/bootstrap.min.js') }}"></script>
    <!-- match-height JS -->
    <script src="{{ asset('front_assets/plugins/match-height/jquery.matchHeight-min.js') }}"></script>
    <!-- Main Script -->
    <script src="{{ asset('front_assets/script.js') }}"></script>
</body>

</html>

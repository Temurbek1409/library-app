@extends('layouts.admin.app')

@section('title', 'Kitoblarni yangilash')

@section('contents')
    {{-- <h1 class="mb-0">Kitoblarni yangilash</h1> --}}
    <hr />
    <form action="{{ route('books.update', $book->id) }}" method="POST">
        @csrf
        @method('PUT')
        <div class="row">
            <div class="col mb-3">
                <label class="form-label">Sarlavhasi</label>
                <input type="text" name="title" class="form-control" placeholder="Sarlavha" value="{{ $book->title }}">
            </div>
            <div class="col mb-3">
                <label class="form-label">ISNB</label>
                <input type="text" name="isbn" class="form-control" placeholder="ISBN" value="{{ $book->isbn }}">
            </div>
        </div>
        <div class="row">
            <div class="col mb-3">
                <label class="form-label">Nashr qilingan sanasi</label>
                <div class="form-group">
                    <input type="text" class="form-control" value="{{ $book->dateOfPublished }}" id="input" />
                </div>

            </div>
            <div class="col mb-3">
                <label class="form-label">Sahifalar soni</label>
                <input type="number" name="pageNumb" class="form-control" value="{{ $book->pageNumb }}">
            </div>
        </div>
        <div class="row">
            <div class="col mb-3">
                <label class="form-label">Kitob yozilgan tili</label>
                <div class="input-group date" id="id_0">
                    <input type="text" name="language" value="{{ $book->language }}" class="form-control"d />
                </div>
            </div>
            <div class="col mb-3">
                <label class="form-label">Kitob haqida tarif</label>
                <input type="text" name="description" class="form-control" value="{{ $book->description }}">
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="d-grid">
                    <button class="btn btn-primary">Update</button>
                </div>
            </div>
        </div>
    </form>
@endsection

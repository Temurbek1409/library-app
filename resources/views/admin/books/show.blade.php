@extends('layouts.admin.app')

@section('title', "Kitob haqida to'liq ma'lumot")

@section('contents')
    {{-- <h1 class="mb-0">Kitob haqida</h1> --}}
    <hr />
    <div class="row">
        <div class="col mb-3">
            <label class="form-label">Sarlavhasi</label>
            <input type="text" name="title" class="form-control" placeholder="Sarlavha" value="{{ $book->title }}"
                readonly>
        </div>
        <div class="col mb-3">
            <label class="form-label">ISNB</label>
            <input type="text" name="isnb" class="form-control" placeholder="ISNB raqami" value="{{ $book->isbn }}"
                readonly>
        </div>
    </div>
    <div class="row">
        <div class="col mb-3">
            <label class="form-label">Nashr qilingan sanasi</label>
            <input type="text" value="{{ $book->dateOfPublished }}" class="form-control" readonly />
        </div>
        <div class="col mb-3">
            <label class="form-label">Sahifalar soni</label>
            <input type="number" name="pageNumb" class="form-control" value="{{ $book->pageNumb }}" readonly>
        </div>
    </div>
    <div class="row">
        <div class="col mb-3">
            <label class="form-label">Kitob yozilgan tili</label>
            <div class="input-group date" id="id_0">
                <input type="text" name="language" value="{{ $book->language }}" class="form-control" readonly />
            </div>
        </div>
        <div class="col mb-3">
            <label class="form-label">Kitob haqida tarif</label>
            <input type="text" name="description" class="form-control" value="{{ $book->description }}" readonly>
        </div>
    </div>
    <div class="row">
        <div class="col mb-3">
            <label class="form-label">Yaratilgan</label>
            <input type="text" name="created_at" class="form-control" value="{{ $book->created_at }}" readonly>
        </div>
        <div class="col mb-3">
            <label class="form-label">Yangilangan</label>
            <input type="text" name="updated_at" class="form-control" value="{{ $book->updated_at }}" readonly>
        </div>
    </div>
@endsection

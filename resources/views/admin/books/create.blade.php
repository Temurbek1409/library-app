@extends('layouts.admin.app')

@section('title', 'Create books')
@section('contents')
    {{-- <h1 class="mb-0">Kitob qo'shish</h1> --}}
    <hr />
    <form action="{{ route('books.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="row mb-3">
            <div class="col">
                <input type="text" name="title" class="form-control" placeholder="Sarlavha">
            </div>
            <div class="col">
                <input type="text" name="isbn" class="form-control" placeholder="ISBN raqami">
            </div>
        </div>
        <div class="row mb-3">
            <div class="col">
                <input type="text" name="dateOfPublished" placeholder="Nashr sanasi" class="form-control"
                    id="input" />
            </div>
            <div class="col">
                <input type="number" name="pageNumb" class="form-control" placeholder="Sahifalar soni">
            </div>
        </div>
        <div class="row mb-3">
            <div class="col">
                <input type="text" name="language" class="form-control" placeholder="Adabiyot tili">
            </div>
            <div class="col">
                <textarea class="form-control" name="description" placeholder="Tarif"></textarea>
            </div>
        </div>

        <div class="row mb-3">
            <div class="d-grid">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
@endsection

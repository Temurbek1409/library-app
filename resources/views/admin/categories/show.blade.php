@extends('layouts.admin.app')

@section('title', "Kategoriya haqida to'liq ma'lumot")

@section('contents')
    {{-- <h1 class="mb-0">Kitob haqida</h1> --}}
    <hr />
    <div class="row">
        <div class="col mb-3">
            <label class="form-label">Nomi</label>
            <div class="input-group date" id="id_0">
                <input type="text" name="name" value="{{ $category->name }}" class="form-control" readonly />
            </div>
        </div>
        <div class="col mb-3">
            <label class="form-label">Ta'rif</label>
            <input type="text" name="description" class="form-control" value="{{ $category->description }}" readonly>
        </div>
    </div>
    <div class="row">
        <div class="col mb-3">
            <label class="form-label">Yaratilgan</label>
            <input type="text" name="created_at" class="form-control" value="{{ $category->created_at }}" readonly>
        </div>
        <div class="col mb-3">
            <label class="form-label">Yangilangan</label>
            <input type="text" name="updated_at" class="form-control" value="{{ $category->updated_at }}" readonly>
        </div>
    </div>
@endsection

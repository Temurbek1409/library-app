@extends('layouts.admin.app')

@section('title', 'Kategoriya qoshish')

@section('contents')
    {{-- <h1 class="mb-0">Kitob qo'shish</h1> --}}
    <hr />
    <form action="{{ route('categories.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="row mb-3">
            <div class="col">
                <input type="text" name="name" class="form-control" placeholder="Nomi">
            </div>
            <div class="col">
                <textarea class="form-control" name="description" placeholder="Tarif"></textarea>
            </div>
        </div>

        <div class="row mb-3">
            <div class="d-grid">
                <button type="submit" class="btn btn-primary">Yaratish</button>
            </div>
        </div>
    </form>
@endsection

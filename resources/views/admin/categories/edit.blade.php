@extends('layouts.admin.app')

@section('title', 'Kategoriyani yangilash')

@section('contents')
    {{-- <h1 class="mb-0">Kitoblarni yangilash</h1> --}}
    <hr />
    <form action="{{ route('categories.update', $category->id) }}" method="POST">
        @csrf
        @method('PUT')

        <div class="row">
            <div class="col mb-3">
                <label class="form-label">Kategoriya nomi</label>
                <div class="input-group date" id="id_0">
                    <input type="text" name="name" value="{{ $category->name }}" class="form-control"d />
                </div>
            </div>
            <div class="col mb-3">
                <label class="form-label">Kategoriya haqida tarif</label>
                <input type="text" name="description" class="form-control" value="{{ $category->description }}">
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="d-grid">
                    <button class="btn btn-primary">Update</button>
                </div>
            </div>
        </div>
    </form>
@endsection

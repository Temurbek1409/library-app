@extends('layouts.admin.app')

@section('title', "Muallif haqida to'liq ma'lumot")

@section('contents')
    {{-- <h1 class="mb-0">Kitob haqida</h1> --}}
    <hr />
    <div class="row mb-3">
        <div class="col">
            <input type="text" name="firstName" value="{{ $author->firstName }}" readonly class="form-control"
                placeholder="ISM">
        </div>
        <div class="col">
            <input type="text" name="lastName" value="{{ $author->lastName }}" readonly class="form-control"
                placeholder="Familiya">
        </div>
    </div>
    <div class="row mb-3">
        <div class="col">
            <input type="email" name="email" value="{{ $author->email }}" readonly class="form-control"
                placeholder="Mail">
        </div>
        <div class="col">
            <input type="text" name="phoneNumber" value="{{ $author->phoneNumber }}" readonly class="form-control"
                placeholder="Tel raqam">
        </div>
    </div>
    <div class="row mb-3">
        <div class="col">
            <textarea class="form-control" readonly>
                {{ $author->description }}
            </textarea>
        </div>
    </div>
    <div class="row">
        <div class="col mb-3">
            <label class="form-label">Yaratilgan</label>
            <input type="text" name="created_at" class="form-control" value="{{ $author->created_at }}" readonly>
        </div>
        <div class="col mb-3">
            <label class="form-label">Yangilangan</label>
            <input type="text" name="updated_at" class="form-control" value="{{ $author->updated_at }}" readonly>
        </div>
    </div>
@endsection

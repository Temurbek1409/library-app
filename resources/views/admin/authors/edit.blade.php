@extends('layouts.admin.app')

@section('title', 'Muallif yangilash')

@section('contents')
    <hr />
    <form action="{{ route('authors.update', $author->id) }}" method="POST">
        @csrf
        @method('PUT')
        <div class="row mb-3">
            <div class="col">
                <input type="text" name="firstName" value="{{ $author->firstName }}" class="form-control" placeholder="ISM">
            </div>
            <div class="col">
                <input type="text" name="lastName" value="{{ $author->lastName }}" class="form-control"
                    placeholder="Familiya">
            </div>
        </div>
        <div class="row mb-3">
            <div class="col">
                <input type="email" name="email" value="{{ $author->email }}" class="form-control" placeholder="Mail">
            </div>
            <div class="col">
                <input type="text" name="phoneNumber" value="{{ $author->phoneNumber }}" class="form-control"
                    placeholder="Tel raqam">
            </div>
        </div>
        <div class="row mb-3">
            <div class="col">
                <textarea class="form-control" name="description">
                    {{ $author->description }}
                </textarea>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="d-grid">
                    <button class="btn btn-primary">Update</button>
                </div>
            </div>
        </div>
    </form>
@endsection

@extends('layouts.admin.app')

@section('title', 'Mualliflar royhati')

@section('contents')
    <div class="d-flex align-items-center justify-content-between">
        {{-- <h1 class="mb-0">Kitoblar ro'yhati</h1> --}}
        <a href="{{ route('authors.create') }}" class="btn btn-primary">Muallif qo'shing</a>
    </div>
    <hr />
    @if (Session::has('success'))
        <div class="alert alert-success" role="alert">
            {{ Session::get('success') }}
        </div>
    @endif
    <table class="table table-hover">
        <thead class="table-primary">
            <tr>
                <th>#</th>
                <th>firstName</th>
                <th>lastName</th>
                <th>email</th>
                <th>phoneNumber</th>
                <th>action</th>
            </tr>
        </thead>
        <tbody>+
            @if ($author->count() > 0)
                @foreach ($author as $rs)
                    <tr>
                        <td class="align-middle">{{ $loop->iteration }}</td>
                        <td class="align-middle">{{ $rs->firstName }}</td>
                        <td class="align-middle">{{ $rs->lastName }}</td>
                        <td class="align-middle">{{ $rs->email }}</td>
                        <td class="align-middle">{{ $rs->phoneNumber }}</td>
                        <td class="align-middle">
                            <div class="btn-group" role="group" aria-label="Basic example">
                                <a href="{{ route('authors.show', $rs->id) }}" type="button"
                                    class="btn btn-secondary">Ma'lumot</a>
                                <a href="{{ route('authors.edit', $rs->id) }}" type="button"
                                    class="btn btn-warning">Yangilash</a>
                                <form action="{{ route('authors.destroy', $rs->id) }}" method="POST" type="button"
                                    class="btn btn-danger p-0" onsubmit="return confirm('Delete?')">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger m-0">O'chirish</button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td class="text-center" colspan="5">Product not found</td>
                </tr>
            @endif
        </tbody>
    </table>
@endsection

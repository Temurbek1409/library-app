@extends('layouts.admin.app')

@section('title', 'Muallif yaratish')
@section('contents')
    <hr />
    <form action="{{ route('authors.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="row mb-3">
            <div class="col">
                <input type="text" name="firstName" class="form-control" placeholder="ISM">
            </div>
            <div class="col">
                <input type="text" name="lastName" class="form-control" placeholder="Familiya">
            </div>
        </div>
        <div class="row mb-3">
            <div class="col">
                <input type="email" name="email" class="form-control" placeholder="Mail">
            </div>
            <div class="col">
                <input type="text" name="phoneNumber" class="form-control" placeholder="Tel raqam">
            </div>
        </div>
        <div class="row mb-3">
            <div class="col">
                <textarea class="form-control" name="description" placeholder="Tarif"></textarea>
            </div>
        </div>

        <div class="row mb-3">
            <div class="d-grid">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
@endsection

@extends('layouts.client.app')
@section('style')
    <style>

    </style>
@endsection
@section('content')
    <section class="section">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 text-center">
                    <h2 class="section-title">Kitoblarni kategoriyasi boyicha izlash</h2>
                </div>
                <!-- topic-item -->
                @if ($book->count() > 0)
                    @foreach ($book as $rs)
                        <div class="col-lg-4 col-sm-6 mb-4">
                            <a href="single.html" class="px-4 py-5 bg-white shadow text-center d-block match-height">
                                <i class="ti-bookmark-alt icon text-primary d-block mb-4"></i>
                                <h3 class="mb-3 mt-0">{{ $rs->title }}</h3>
                                <p class="mb-0">
                                    {{ strlen($rs->description) > 20 ? substr($rs->description, 0, 20) . '...' : $rs->description }}
                                </p>
                            </a>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </section>
    <!-- /topics -->

    <!-- call to action -->
    <section>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section px-3 bg-white shadow text-center">
                        <h2 class="mb-4">Taklif va mulohazalar uchun</h2>
                        <p class="mb-4">elеktron kutubxonalar uchun elеktron katalog yaratish.</p>
                        <a th:href="@{/contact}" class="btn btn-primary">biz bilan bog'laning</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

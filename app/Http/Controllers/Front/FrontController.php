<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Books;

class FrontController extends Controller
{
    public function index()
    {
        $book = Books::orderBy('created_at', 'DESC')->get();
        return view('client.index', compact('book'));
    }
}

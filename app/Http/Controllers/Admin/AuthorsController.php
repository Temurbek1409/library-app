<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Authors;
use Illuminate\Http\Request;

class AuthorsController extends Controller
{
    public function index()
    {
        $author = Authors::orderBy('created_at', 'DESC')->get();
        return view('admin.authors.index', compact('author'));
    }
    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.authors.create');
    }
    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        Authors::create($request->all());

        // Redirect to the Authors index route with a success message
        return redirect()->route('authors')->with('success', 'Yangi muallif qoshildi');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $author = Authors::findOrFail($id);

        return view('admin.authors.show', compact('author'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $author = Authors::findOrFail($id);

        return view('admin.authors.edit', compact('author'));
    }
    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $author = Authors::findOrFail($id);

        $author->update($request->all());

        return redirect()->route('authors')->with('success', 'Kitob yangilandi');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $author = Authors::findOrFail($id);

        $author->delete();

        return redirect()->route('authors')->with('success', "Kitob o'chirildi");
    }
}
